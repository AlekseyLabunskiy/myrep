<?php
session_start();
header("Content-Type: image/png");

$width = 110;
$height = 60;
$font_size = 16;
$amount = 4;
$font = "arial.ttf";
$letters = array('1','2','3','4','5','6','7','8','9','0','A','B','C','D','E','F','J','H','I','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
$colors = array("100","110","120","130","140","255","146","154");

$src = imagecreatetruecolor($width,$height);
$fon = imagecolorallocate($src,255,255,255);
imagefill($src,0,0,$fon);
for($i=0;$i<$amount;$i++){
    $color= imagecolorallocatealpha($src,$colors[rand(0,sizeof($colors)-1)],$colors[rand(0,sizeof($colors)-1)],$colors[rand(0,sizeof($colors)-1)],5);
    $size = rand($font_size*2-2,$font_size*2+2);
    $letter = $letters[rand(0,sizeof($letters)-1)];
    $x= ($i+1)*$font_size + rand(0,10);
    $y = (($height*2)/3 + rand(0,5));
  imagettftext($src,$size,rand(-15,15),$x,$y,$color,$font,$letter);  
  $cap[]=$letter;
}
for($i=0;$i<5;$i++){
    $x1 = rand(0,100);
    $y1 = rand(0,100);
    $x2= rand(0,100);
    $y2= rand(0,50); 
    $colorforlines= array("55","110","125","20","150","175","60");
    $color = imagecolorallocate($src,$colorforlines[rand(0,sizeof($colorforlines)-1)],$colorforlines[rand(0,sizeof($colorforlines)-1)],$colorforlines[rand(0,sizeof($colorforlines)-1)]);
    imageline($src,$x1,$y1,$x2,$y2,$color);
}
for($i=0;$i<5;$i++){
    $x1= rand(0,100);
    $y1= rand(0,100);
    $x2= rand(5,30);
    $y2= rand(5,30);
    $x3= rand(0,100);
    $y3= rand(0,100);
    $color = imagecolorallocate($src,$colorforlines[rand(0,sizeof($colorforlines)-1)],$colorforlines[rand(0,sizeof($colorforlines)-1)],$colorforlines[rand(0,sizeof($colorforlines)-1)]);
    imagearc($src,$x1,$y1,$x2,$y2,$x3,$y3,$color);
}

$cap = implode("",$cap);
$_SESSION['cap'] = $cap;
imagepng($src);
imagedestroy($src);

?>