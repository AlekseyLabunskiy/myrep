<?php
//
// Менеджер відео
//
class M_Video
{
	private static $instance; 	// ссылка на экземпляр класса
	private $msql; 				// драйвер БД
	
	//
	// Получение единственного экземпляра (одиночка)
	//
	public static function Instance()
	{
		if (self::$instance == null)
			self::$instance = new M_Video();
		
		return self::$instance;
	}

	//
	// Конструктор
	//
	public function __construct()
	{
		$this->msql = new M_WMSQL();
	}
	
	//
	// Список всех відео
	//
	public function All_video()
	{
		$query = "SELECT * FROM video ORDER BY id DESC";
		$result = array();		  
       $result = $this->msql->Select($query);
       return $result;
	}
	
    public function All_video_limit()
	{
		$query = "SELECT * FROM video ORDER BY id DESC LIMIT 3";
		$result = array();		  
       $result = $this->msql->Select($query);
       return $result;
	}
	//
	// Конкретне відео
	//
	public function Get_one_video($id_video)
	{
		// Запрос.
		$t = "SELECT * FROM video WHERE id = '$id_video'";
			  
		$query = $t;
		$result = $this->msql->Select($query);
		return $result;
	}

	//
	// Добавить відео
	//
public function Add_video($title, $content)
	{
		
		// ????.
		if ($title == '')
			return false;
		
		// ???.
		$obj = array();
		$obj['name'] = $title;
		$obj['link'] = $content;
		$this->msql->Insert('video', $obj);
		return true;
	}
	
    public function Edit_video($id_article, $title, $content)
	{
		// ?????.
		$title = trim($title);
		

		// ????.
		if ($title == '' || $content == '')
			return false;
		
		// ???.
		$obj = array();
		$obj['name'] = $title;
		$obj['link'] = $content;

		
		$t = "id = '%d'";		
		$where = sprintf($t, $id_article);		
		$this->msql->Update('video', $obj, $where);
		return true;
	}    

	public function Delete_video($id_video)
	{
		// Запрос.
		$t = "id = '%d'";		
		$where = sprintf($t, $id_video);		
		$this->msql->Delete('video', $where);
		return true;
	}
}
