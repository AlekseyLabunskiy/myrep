<?php
class M_Users
{	
	private static $instance;	// ýêçåìïëÿð êëàññà
	private $msql;				// äðàéâåð ÁÄ
	private $sid;				// èäåíòèôèêàòîð òåêóùåé ñåññèè
	private $uid;				// èäåíòèôèêàòîð òåêóùåãî ïîëüçîâàòåëÿ
	
	//
	// Ïîëó÷åíèå ýêçåìïëÿðà êëàññà
	// ðåçóëüòàò	- ýêçåìïëÿð êëàññà MSQL
	//
	public static function Instance()
	{
		if (self::$instance == null)
			self::$instance = new M_Users();
			
		return self::$instance;
	}

	//
	// Êîíñòðóêòîð
	//
	public function __construct()
	{
		$this->msql = new M_WMSQL();
		$this->sid = null;
		$this->uid = null;
	}
	public function getAllusers()
    {
        $t = "SELECT * FROM `users`";
            $query = $t;
            $result = $this->msql->Select($query);
            return $result;
    }
		public function Add_user($user_login, $user_name, $user_password)
	{
		// ?????.
		$user_login = trim($user_login);
		$user_name = trim($user_name);
        $user_password = trim($user_password);

		// ????.`
		if ($user_login  == '')
			return false;
		
		// ???.
		$obj = array();
		$obj['login'] = $user_login;
		$obj['name'] = $user_name;
		$obj['password'] = $user_password;
        $obj['id_role'] = 2;
        
		$this->msql->Insert( 'users', $obj);
		return true;
	}

		public function Get_one_user($sid)
	{
		// ???.
		$t = "SELECT * FROM sessions WHERE  sid = '$sid'";
			  
		$query = $t;
		$result = $this->msql->Select($query);
		return $result;
	}

	public function ClearSessions()
	{
		$min = date('Y-m-d H:i:s', time() - 60 * 20); 			
		$t = "time_last < '%s'";
		$where = sprintf($t, $min);
		$this->msql->Delete('sessions', $where);
	}

	//
	// Àâòîðèçàöèÿ
	// $login 		- ëîãèí
	// $password 	- ïàðîëü
	// $remember 	- íóæíî ëè çàïîìíèòü â êóêàõ
	// ðåçóëüòàò	- true èëè false
	//
	public function Login($login, $password, $remember = true)
	{
		// âûòàñêèâàåì ïîëüçîâàòåëÿ èç ÁÄ 
		$user = $this->GetByLogin($login);

		if ($user == null)
			return false;
		
		$id_user = $user['id_user'];
				
		// ïðîâåðÿåì ïàðîëü
		if ($user['password'] != md5(md5($password)))
			return false;
				
		// çàïîìèíàåì èìÿ è md5(ïàðîëü)
		if ($remember)
		{
			$expire = time() + 3600 * 24 * 100;
			setcookie('login', $login, $expire);
			setcookie('password', md5($password), $expire);
		}		
				
		// îòêðûâàåì ñåññèþ è çàïîìèíàåì SID
		$this->sid = $this->OpenSession($id_user);
		
		return true;
	}
	
	//
	// Âûõîä
	//
	public function Logout()
	{
		setcookie('login', '', time() - 1);
		setcookie('password', '', time() - 1);
		unset($_COOKIE['login']);
		unset($_COOKIE['password']);
		unset($_SESSION['sid']);		
		$this->sid = null;
		$this->uid = null;
	}				
	//
	// Ïîëó÷åíèå ïîëüçîâàòåëÿ
	// $id_user		- åñëè íå óêàçàí, áðàòü òåêóùåãî
	// ðåçóëüòàò	- îáúåêò ïîëüçîâàòåëÿ
	//
	public function Get($id_user = null)
	{	
		// Åñëè id_user íå óêàçàí, áåðåì åãî ïî òåêóùåé ñåññèè.
		if ($id_user == null)
			$id_user = $this->GetUid();
			
		if ($id_user == null)
			return null;
			
		// À òåïåðü ïðîñòî âîçâðàùàåì ïîëüçîâàòåëÿ ïî id_user.
		$t = "SELECT * FROM users WHERE id_user = '%d'";
		$query = sprintf($t, $id_user);
		$result = $this->msql->Select($query);
		return $result[0];		
	}
	
	//
	// Ïîëó÷àåò ïîëüçîâàòåëÿ ïî ëîãèíó
	//
	public function GetByLogin($login)
	{	
		$t = "SELECT * FROM users WHERE login = '%s'";
		$query = sprintf($t, mysql_real_escape_string($login));
		$result = $this->msql->Select($query);
		return $result[0];
	}
			
	//
	// Ïðîâåðêà íàëè÷èÿ ïðèâèëåãèè
	// $priv 		- èìÿ ïðèâèëåãèè
	// $id_user		- åñëè íå óêàçàí, çíà÷èò, äëÿ òåêóùåãî
	// ðåçóëüòàò	- true èëè false
	//
	public function Can($priv, $id_user = null)
	{		
		if($id_user == null)
			$id_user = $this->GetUid();
			if($id_user == null){
				return false;
			}
			
				$t = "SELECT * FROM `users` JOIN `privtoroles` USING(id_role) JOIN `priv` USING(id_priv) WHERE priv.name = '$priv' AND users.id_user = '$id_user'";
					$query =sprintf($t,mysql_real_escape_string($priv));
						$result = $this->msql->Select($query);
							return $result[0];
							if($result != null){
								return true;
							}
						
	}


	//
	// Ïðîâåðêà àêòèâíîñòè ïîëüçîâàòåëÿ
	// $id_user		- èäåíòèôèêàòîð
	// ðåçóëüòàò	- true åñëè online
	//
	public function IsOnline($id_user)
	{		
		if($id_user == null)
			$id_user = $this->GetUid();
			if($id_user == null){
				return false;
			}
		if($this->onlineMap == null){
			$t = "SELECT DISTINCT id_user FROM sessions";
				$query = sprintf($t,$id_user);
					$result = $this -> msql-> Select($query);
						foreach ($result as $item) {
							$this->onlineMap[$item['id_user']] = true;
						}
		}
		return $this->onlineMap[$item['id_user']] =! null;
	}
	
	public function getUser(){
		$id_user = $this->GetUid();
		$session = $_SESSION['sid'];
			if($id_user == null){
			$id_user = '0';
				}
			if ($id_user == null) {
					return false;
				}	
				$t = "SELECT * FROM `users` JOIN `sessions` USING(id_user) WHERE sessions.id_user ='$id_user' AND sessions.sid = '$session' AND users.id_user ='$id_user'";
					$result = $this-> msql-> Select($t);
					return $result[0];
	}

	public function unsetCurrentSession(){
		unset($_SESSION['sid']);
		return true; 
	}

	public function GetUid()
	{	
		// Ïðîâåðêà êåøà.
		if ($this->uid != null)
			return $this->uid;	

		// Áåðåì ïî òåêóùåé ñåññèè.
		$sid = $this->GetSid();
				
		if ($sid == null)
			return null;
			
		$t = "SELECT id_user FROM sessions WHERE sid = '%s'";
		$query = sprintf($t, mysql_real_escape_string($sid));
		$result = $this->msql->Select($query);
				
		// Åñëè ñåññèþ íå íàøëè - çíà÷èò ïîëüçîâàòåëü íå àâòîðèçîâàí.
		if (count($result) == 0)
			return null;
			
		// Åñëè íàøëè - çàïîìèíì åå.
		$this->uid = $result[0]['id_user'];
		return $this->uid;
	}

	//
	// Ôóíêöèÿ âîçâðàùàåò èäåíòèôèêàòîð òåêóùåé ñåññèè
	// ðåçóëüòàò	- SID
	//
	private function GetSid()
	{
		// Ïðîâåðêà êåøà.
		if ($this->sid != null)
			return $this->sid;
	
		// Èùåì SID â ñåññèè.
		$sid = $_SESSION['sid'];
								
		// Åñëè íàøëè, ïîïðîáóåì îáíîâèòü time_last â áàçå. 
		// Çàîäíî è ïðîâåðèì, åñòü ëè ñåññèÿ òàì.
		if ($sid != null)
		{
			$session = array();
			$session['time_last'] = date('Y-m-d H:i:s'); 			
			$t = "sid = '%s'";
			$where = sprintf($t, mysql_real_escape_string($sid));
			$affected_rows = $this->msql->Update('sessions', $session, $where);

			if ($affected_rows == 0)
			{
				$t = "SELECT count(*) FROM sessions WHERE sid = '%s'";		
				$query = sprintf($t, mysql_real_escape_string($sid));
				$result = $this->msql->Select($query);
				
				if ($result[0]['count(*)'] == 0)
					$sid = null;			
			}			
		}		
		
		// Íåò ñåññèè? Èùåì ëîãèí è md5(ïàðîëü) â êóêàõ.
		// Ò.å. ïðîáóåì ïåðåïîäêëþ÷èòüñÿ.
		if ($sid == null && isset($_COOKIE['login']))
		{
			$user = $this->GetByLogin($_COOKIE['login']);
			
			if ($user != null && $user['password'] == $_COOKIE['password'])
				$sid = $this->OpenSession($user['id_user']);
		}
		
		// Çàïîìèíàåì â êåø.
		if ($sid != null)
			$this->sid = $sid;
		
		// Âîçâðàùàåì, íàêîíåö, SID.
		return $sid;		
	}
	
	//
	// Îòêðûòèå íîâîé ñåññèè
	// ðåçóëüòàò	- SID
	//
	private function OpenSession($id_user)
	{
		// ãåíåðèðóåì SID
		$sid = $this->GenerateStr(10);
				
		// âñòàâëÿåì SID â ÁÄ
		$now = date('Y-m-d H:i:s'); 
		$session = array();
		$session['id_user'] = $id_user;
		$session['sid'] = $sid;
		$session['time_start'] = $now;
		$session['time_last'] = $now;				
		$this->msql->Insert('sessions', $session); 
				
		// ðåãèñòðèðóåì ñåññèþ â PHP ñåññèè
		$_SESSION['sid'] = $sid;				
				
		// âîçâðàùàåì SID
		return $sid;	
	}

	//
	// Ãåíåðàöèÿ ñëó÷àéíîé ïîñëåäîâàòåëüíîñòè
	// $length 		- åå äëèíà
	// ðåçóëüòàò	- ñëó÷àéíàÿ ñòðîêà
	//
	private function GenerateStr($length = 10) 
	{
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
		$code = "";
		$clen = strlen($chars) - 1;  

		while (strlen($code) < $length) 
            $code .= $chars[mt_rand(0, $clen)];  

		return $code;
	}
}
