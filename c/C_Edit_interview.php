<?php
//
// Контроллер страницы редактирования.
//
class C_Edit_interview extends C_Base_Editor
{
	protected $list_article_content;
	protected $article_content;	
	private $title_art;
	private $content_art;

	//
	// Конструктор.
	//
	function __construct()
	{	
		$this->article_content = array();	
	}
	
	//
	// Виртуальный обработчик запроса.
	//
	protected function OnInput()
	{
		parent::OnInput();
		
		$connect = M_connectDb::connectDb();
        $this->conn = $connect->connectmyDb();

	 	$mUsers = M_Users::Instance();
        $mUsers->ClearSessions();
        $user = $mUsers->Get();
        if (!$mUsers->Can('VIEW_EDIT'))
		{
			header('location:/index.php');
		}

		$this->title = $this->title . ' :: Редагування';
		
		if (isset($_GET['c']) && $_GET['c'] == 'edit_interview') {
			$mArticles = M_Articles::Instance();
			 $this ->list_article_content = $mArticles->All_interv_articles();
		
			if (isset($_GET['id_interv'])) {
				$mArticles = M_Articles::Instance();
			 	$id_article = trim(htmlspecialchars(stripcslashes($_GET['id_interv'])));
					$this->cont = $mArticles -> Get_one_interv_article($id_article);
						foreach ($this->cont as $cont) {
							$this->article_content['name'] = $cont['name'];
							$this->article_content['content'] = $cont['content'];
							$this->article_content['image'] = $cont['image'];
							$this->article_content['author'] = $cont['author'];
						}
			}
			if (isset($_GET['delete_interv'])){
				$id_article = trim(htmlspecialchars(stripcslashes($_GET['id_interv'])));
				if ($mArticles->Delete_interv($id_article)) {
					header('location:/index.php?c=edit_interview');
					die();
				}
			}
		}
			if (isset($_POST['edit_interv'])) {
				$mArticles = M_Articles::Instance();
					$id_article = trim(htmlspecialchars(stripcslashes($_GET['id_interv'])));
					$title = trim(htmlspecialchars(stripcslashes($_POST['title_interv'])));
					$content = trim(htmlspecialchars(stripcslashes($_POST['content_interv'])));
				

					if($mArticles-> Edit_interview_artiles($id_article, $title, $content)){
						header('location:/index.php?c=edit_interview');
						die();
					}

			}
            if(isset($_POST['load_file'])){
                if($_FILES["filename"]["size"] = 0){
                    echo"загрузіть файл!";
                }
                
                if($_FILES["filename"]["size"] > 1024*3*1024){
                    echo ("Размер файла превышает три мегабайта");
                        exit;
                }
              if($mArticles->addImagesInterview()){
                 header('location:/index.php?c=edit_interview');
                    die();
              }
            }               
		
	}
	
	//
	// Виртуальный генератор HTML.
	//	
	protected function OnOutput()
	{
	   	$this->vars_content = array('content'=>$this ->list_article_content,'news_art'=>$this->article_content['name'],'content_art'=>$this->article_content['content'],'image_art'=>$this->article_content['image'],'author'=>$this->article_content['aythor']);	
	      $this->content = $this->Template('v/v_edit_interview.php', $this->vars_content);
		parent::OnOutput();
	}	
}
