<?php
//
// Контроллер страницы редактирования.
//
class C_Edit_add_top_articles extends C_Base_Editor
{
	protected $list_article_content;
	protected $article_content;	
	private $title_art;
	private $content_art;
	private $error_mesage;
	private $error;

	//
	// Конструктор.
	//
	function __construct()
	{	
		$this->article_content = array();	
	}
	
	//
	// Виртуальный обработчик запроса.
	//
	protected function OnInput()
	{
		parent::OnInput();
		
		$connect = M_connectDb::connectDb();
        $this->conn = $connect->connectmyDb();

	 	$mUsers = M_Users::Instance();
        $mUsers->ClearSessions();
        $user = $mUsers->Get();
        if (!$mUsers->Can('VIEW_EDIT'))
		{
			header('location:/index.php');
		}

		$this->title = $this->title . ' :: Редагування';
		
		if (isset($_GET['c']) && $_GET['c'] == 'edit_add_top_articles') {
			if (isset($_POST['add_top_article'])) {
				$this->error = '';
					$this->error_mesage = '';

					if ($_POST['add_title_top_art'] == '') {
						$this->error = true;
						$this->error_mesage = 'Необхідно ввести заголовок новини';
					}
					if ($_POST['add_content_top_article'] == '') {
						$this->error = true;
						$this->error_mesage = 'Необхідно ввести вміст новини';
					}
					if ($_POST['add_author_top_art'] == '') {
						$this->error = true;
						$this->error_mesage = 'Необхідно ввести автора новини';
					}
						if (!$this->error) {
							$title =trim(htmlspecialchars(stripcslashes($_POST['add_title_top_art'])));
							$content = trim(htmlspecialchars(stripcslashes($_POST['add_content_top_article'])));
							$author = trim(htmlspecialchars(stripcslashes($_POST['add_author_top_art'])));
							$image = trim(htmlspecialchars(stripcslashes($_POST['add_image_top_art'])));
							$mArticles = M_Articles::Instance();
							if ($mArticles -> Add_top_articles($title, $content,$author,$image)) {
								$this->error_mesage = 'Новина успішно добавлена!';
								if ($mArticles -> Top_News_Add_To_News()) {
								        
										$mArticles -> Delete_one_top_news();
                                        	header('/index.php?c=edit_add_top_articles');	
                                    }
								
							}
							
						}
			}

		}
		
	}
	
	//
	// Виртуальный генератор HTML.
	//	
	protected function OnOutput()
	{
		$this->vars_content = array('error_mesage' => $this->error_mesage);	
		$this->content = $this->Template('v/v_edit_add_top_news.php', $this->vars_content);
		parent::OnOutput();
	}	
}
