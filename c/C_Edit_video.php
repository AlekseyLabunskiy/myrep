<?php
//
// Контроллер страницы редактирования.
//
class C_Edit_video extends C_Base_Editor
{
	protected $list_article_content;
	protected $article_content;	
	private $title_art;
	private $content_art;

	//
	// Конструктор.
	//
	function __construct()
	{	
		$this->article_content = array();	
	}
	
	//
	// Виртуальный обработчик запроса.
	//
	protected function OnInput()
	{
		parent::OnInput();
		
		$connect = M_connectDb::connectDb();
        $this->conn = $connect->connectmyDb();
        $this->title = 'Консоль';
	 	$mUsers = M_Users::Instance();
        $user = $mUsers->Get();
        if (!$mUsers->Can('VIEW_EDIT'))
		{
			header('location:/index.php');
		}

		$this->title = $this->title . ' :: Редагування';
		
		if (isset($_GET['c']) && $_GET['c'] == 'edit_video') {
			$mArticles = M_Video::Instance();
			  $this ->list_video_content = $mArticles->All_video();
			 if (isset($_GET['id_video'])) {
				$mArticles = M_Video::Instance();
			 	$id_video = $_GET['id_video'];
					$this->cont = $mArticles -> Get_one_video($id_video);
						foreach ($this->cont as $cont) {
							$this->article_content['name'] = $cont['name'];
							$this->article_content['link'] = $cont['link'];
						}
			}
        	if (isset($_GET['delete_video'])){
			     $id_video = $_GET['id_video'];
				    if ($mArticles->Delete_video($id_video)) {
				    	header('location:/index.php?c=edit_video');
					   die();
				}
		}
		}
			if (isset($_POST['edit_video'])) {
				$mArticles = M_Video::Instance();
					$id_article = $_GET['id_video'];
					$title = $_POST['title_video'];
					$content = $_POST['content_video'];

					if($mArticles-> Edit_video($id_article, $title, $content)){
						header('location:/index.php?c=edit_video');
						die();
					}

			}
		
	}
	
	//
	// Виртуальный генератор HTML.
	//	
	protected function OnOutput()
	{ 
	   $this->vars_content = array('content'=>$this ->list_video_content,'news_art'=>$this->article_content['name'],'content_art'=>$this->article_content['link']);	  
	       $this->content = $this->Template('v/v_edit_video.php', $this->vars_content);
            $this->vars = array('title' => $this->title, 'content' => $this -> content,'right_bar' =>$this->edit_right_bar,'reg_user'=>$this->reg_user,'online' =>$this->online,'user'=>$this->user);	 	
                 parent::OnOutput();
	}	
}
?>