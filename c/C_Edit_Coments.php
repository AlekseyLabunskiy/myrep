<?php
class C_Edit_Coments extends C_Base_Editor
{
    private $all_users;
    private $users;
    private $user_all_coments;
    private $one_coment;
    
    
    function __construct()
	{		
	}
    
    protected function OnInput()
	{
	   	parent::OnInput();
        $mUsers = M_Users::Instance();
        $mUsers->ClearSessions();
        $this->user = $mUsers->Get();
        if (!$mUsers->Can('VIEW_EDIT'))
		{
			header('location:/index.php');
		}
         $this->all_users = $mUsers->getAllusers();
         if(isset($_POST['user_serch'])){
            $mComents = M_User_coments ::Instance();
                $id_user = $_POST['user_serch'];
            $this->user_all_coments = $mComents -> getAllcomentsOneUser($id_user);
         }
         if(isset($_GET['id_coment'])){
            $id_coment = $_GET['id_coment']; 
                $mComents = M_User_coments ::Instance();
                    $this->one_coment = $mComents->getOneComent($id_coment);
         }
         if(isset($_POST['submit_edit_coments'])){
            $mComents = M_User_coments ::Instance();
                $id_article = $_GET['id_coment'];
                    $content = $_POST['edit_coments'];
                        $mComents->Edit_coment($id_article,$content);
                            $this->mesage = 'коментар змінено!';
                                header( 'Refresh: 2; url=/index.php?c=edit_coments' ); 
         }

         
    }
    	protected function OnOutput()
	{
        $this->vars_content = array('mesage'=>$this->mesage,'content_coment'=>$this->one_coment[0]['coments'],'user_all_coments'=>$this->user_all_coments,'all_users'=> $this->all_users);
            $this->content = $this->Template('v/v_edit_coments.php', $this->vars_content);       
        parent::OnOutput();
	}
}
?>