<?php
//
// Базовый контроллер сайта.
//
 class C_View_interest_articles extends C_Base
{
	protected $title;		// заголовок страницы
	protected $content;
    protected $one_int_article;
    protected $list_int_art;		// содержание страницы

	//
	// Конструктор.
	//
	function __construct()
	{		
	}
	
	//
	// Виртуальный обработчик запроса.
	//
	protected function OnInput()
	{
		parent::OnInput();
            $this->title = 'КОРИСНО ЗНАТИ';
            $this->title_rb = 'ВІДЕО НОВИНИ';
            
            $connect = M_connectDb::connectDb();
            $this->conn = $connect->connectmyDb();

        $mUsers = M_Users::Instance();
        $mUsers->ClearSessions();
        $user = $mUsers->Get();
            
             
                if(isset($_GET['id_int_art'])){
                    $id_article = (int)$_GET['id_int_art'];
                    $mArticles = M_Articles::Instance();
                    $this->one_int_article = $mArticles->Get_one_int_article($id_article);
                    $this->list_int_art = $mArticles->All_interesting_articles();
                    
             $mComents = M_User_coments ::Instance();
            $this->list_coments = $mComents -> All_coments($id_news);
            if(isset($_POST['coments'])){
                 if (!isset($_SESSION['sid'])) {
                 $this->error_coment ='що б залишати коментарі треба авторизуватися';
               }
               $content = trim(htmlspecialchars(stripcslashes($_POST['add_coment'])));
               $mUsers = M_Users::Instance();

               $this->user = $mUsers->getUser();
               $name_user = $this->user['login'];
               $id_user = $this->user['id_user'];
            
            if($name_user == ''){$this->error_coment ='що б залишати коментарі треба авторизуватися';}
            
               $id_news = trim(htmlspecialchars(stripcslashes($_GET['id_int_art'])));  
               $description = 'ONE_INTEREST_ARTICLE';
               $mComents = M_User_coments ::Instance(); 
               if (!empty($_POST['add_coment'])) {
               $this->add_coment = $mComents -> Add_coment($content, $name_user, $id_news,$id_user,$description);
             }
           }
           $id_news = $id_article;
           $mComents = M_User_coments ::Instance();
           $this->list_coments = $mComents -> All_coments($id_news);                                        
                }
                       
            $time = new main_Time;
            $this->main_time = $time-> mainTime();
            
            $mVideo = M_Video::Instance();
            $this->video = $mVideo->All_video();


	}
	
	//
	// Виртуальный генератор HTML.
	//	
	protected function OnOutput()
	{
		$vars = array('error_coment'=>$this->error_coment,'time' =>$this->main_time, 'title' =>$this->title,'one_int_article'=>$this->one_int_article,'list_int_art'=>$this->list_int_art,'list_coment'=> $this->list_coments);	 
		$this->content = $this->Template('v/v_one_int_news.php', $vars);
        $this-> vars_right_bar  = array('title'=>$this->title_rb, 'video'=>$this->video);
		parent::OnOutput();
	}	
}



