﻿<?php
class C_View_one_top_news extends C_Base{
       private $one_top_news;
       private $list_news;

	//
	// Конструктор.
	//
	function __construct()
	{		
	}                                                   
	//
	// Виртуальный обработчик запроса.
	//
	protected function OnInput()
	{
	   parent :: OnInput();

        $mUsers = M_Users::Instance();
        $mUsers->ClearSessions();
        $user = $mUsers->Get();
        $this->title_rb = 'ІНШІ НОВИНИ';

            if(isset($_GET['id_top_news'])){
                 $id_article = (int)$_GET['id_top_news'];
                            $mArticles = M_Articles::Instance();
                            $this->one_top_news = $mArticles->Get_one_top_news($id_article);  
                            $this->list_news = $mArticles->All_articles();
        
                            
             $mComents = M_User_coments ::Instance();
            $this->list_coments = $mComents -> All_coments($id_news);
            if(isset($_POST['coments'])){
                 if (!isset($_SESSION['sid'])) {
                 $this->error_coment ='що б залишати коментарі треба авторизуватися';
               }
               $content = trim(htmlspecialchars(stripcslashes($_POST['add_coment'])));

               $mUsers = M_Users::Instance();

               $this->user = $mUsers->getUser();
               if ($this->user == null) {
                 $this->error_coment = 'Авторизуйтесь';
               }
               $name_user = $this->user['login'];
               $id_user = $this->user['id_user'];
            
            if($name_user == ''){$this->error_coment ='що б залишати коментарі треба авторизуватися';}
            
               $id_news = trim(htmlspecialchars(stripcslashes($_GET['id_top_news'])));  
               $description = 'ONE_TOP_NEWS';
               $mComents = M_User_coments ::Instance(); 
               if (!empty($_POST['add_coment'])) {
                    $this->add_coment = $mComents -> Add_coment($content, $name_user, $id_news,$id_user,$description);
               }
               else{
                  $this->error_coment = 'Добавте коментар';
               }          
           }
           $id_news = $id_article;
           $mComents = M_User_coments ::Instance();
           $this->list_coments = $mComents -> All_coments($id_news);
            }
                   
       
       
	}
	
	//
	// Виртуальный генератор HTML.
	//	
	protected function OnOutput()
	{
	    $vars = array('title'=>$this->title,'content' =>$this->one_top_news,'list_coment'=> $this->list_coments,'error_coment'=>$this->error_coment);
        $this->content = $this->Template('v/v_one_top_news.php',$vars); 
        $this-> vars_right_bar = array('title'=>$this->title_rb,'list_news'=>$this->list_news);
       parent::OnOutput();
	}
 
}

?>