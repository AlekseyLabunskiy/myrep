<?php
class C_View_list_interview extends C_Base
{
    private $list_all_interv_art;
    function __construct()
	{	
	}
    
    
	protected function OnInput()
	{
	   
	   parent::OnInput();
        $mArticles = M_Articles::Instance();
        $this->list_all_interv_art = $mArticles->All_interv_articles(); 
       
        $mVideo = M_Video::Instance();
        $this->video = $mVideo->All_video_limit();
        $this->interesting_articles = $mArticles->All_interesting_articles();

    }
    
	protected function OnOutput()
	{
		$vars = array('list_interv_art'=>$this->list_all_interv_art,'pattern'=>$this->pattern);	 
		$this->content = $this->Template('v/v_list_interview.php', $vars);
        $this-> vars_right_bar = array('title'=>$this->title_rb,'pattern'=>$this->pattern, 'video'=>$this->video,'list_interesting_art'=>$this->interesting_articles,'title_int_articles'=>$this->title_int_art);
        parent::OnOutput();
       
	}
}
?>