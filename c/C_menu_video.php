﻿<?php
//
// Базовый контроллер сайта.
//
class C_menu_video extends C_Base
{
	protected $title;		// заголовок страницы
	protected $list_video;	
    protected $list_articles;
    protected $user_session;

	function __construct()
	{	
	   
	}
	
	//
	// Виртуальный обработчик запроса.
	//
	protected function OnInput()
	{
	   parent :: OnInput();
	   $connect = M_connectDb::connectDb();
       $this->conn = $connect->connectmyDb();

   	 	$mUsers = M_Users::Instance();
        $mUsers->ClearSessions();
        $user = $mUsers->Get();
      
      $mVideo = M_Video::Instance();
          $this->title = 'ВСЕ ВІДЕО';
          $this->title_rb = 'ВСІ НОВИНИ';
          
          $this->list_video = $mVideo->All_video(); 
        
      $mArticles = M_Articles::Instance();
        $this->list_articles = $mArticles -> All_articles();
      $time = new main_Time;
        $this->main_time = $time-> mainTime();
    }
	
	//
	// Виртуальный генератор HTML.
	//	
	protected function OnOutput()
	{
		$vars = array('time'=>$this->main_time,'title'=>$this->title,'list_video' =>$this->list_video);	
        $this->content = $this->Template('v/v_menu_video.php', $vars);
        
        $this-> vars_right_bar = array('title'=>$this->title_rb,'list_news'=>$this->list_articles);
		parent::OnOutput();
	}	
}
