﻿<?php

class C_User_registration extends C_Base
{
    private $news;
    private $main_time;
    private $video;
    private $interesting_articles;
    private $list_interview;
    private $error;
    private $messageerror;
    private $user_check;
    
	//
	// Конструктор.
	//
	function __construct()
	{	
	}
	
	//
	// Виртуальный обработчик запроса.
	//
	protected function OnInput()
	{
		parent::OnInput();
            $this->title = 'ВСІ НОВИНИ';
            $this->title_rb = 'ВІДЕО НОВИНИ';
            $this->title_int_art = 'КОРИСНО ЗНАТИ';
            $this->title_interv = 'ІНТЕРВ\'Ю';
              $this->pattern = '/([A-ZА-Я]+.+)[.!?]+[\s]+/sU';
            $mUsers = M_Users::Instance();
            $mUsers->ClearSessions();
            $mUsers->Logout();
            $user = $mUsers->Get();
                
            $mArticles = M_Articles::Instance();
            $this->news = $mArticles->All_articles();

            $this->interesting_articles = $mArticles->All_interesting_articles();
            $this->list_interview = $mArticles ->All_interv_articles();
           
            $time = new main_Time;
            $this->main_time = $time-> mainTime();
 
            $mVideo = M_Video::Instance();
            $this->video = $mVideo->All_video();
                
            if(isset($_POST['submit_user_reg'])){
                $user_login = $_POST['user_login'];
                    $user_name = $_POST['user_name'];
                         $user_password = $_POST['user_password'];
                            
                            $user_login = stripcslashes(htmlspecialchars($_POST['user_login']));
                            $user_name = stripcslashes(htmlspecialchars($_POST['user_name']));
                            $user_password = md5(md5(stripcslashes(htmlspecialchars($_POST['user_password']))));
                            
                            $this->error = false;
                            $this->messageerror = '';
                            
                            if(strlen($user_login) == ''){
                                $this->error = true;
                                    $this->messageerror = 'Введіть Логін';
                            }
                            if(strlen($user_name) == ''){
                                $this->error = true;
                                    $this->messageerror = 'Введіть И\'мя';
                            }
                            if(strlen($user_password) == ''){
                                $this->error = true;
                                    $this->messageerror = 'Введіть пароль';
                            }
                            if(!$this->error){
                                $mUser = M_Users :: Instance();
                                $this->user_check = $mUser-> Get_one_user($user_login);
                                    if(count($this->user_check)!= 0){
                                        $this->messageerror = 'Користувач з таким логіном вже їснує!<br/> Спробуйте ще раз.';
                                    }
                                    else{
                                        $mUser = M_Users :: Instance();
                                            $this->add_user = $mUser -> Add_user($user_login, $user_name, $user_password);
                                            if($this->add_user){
                                                $this->messageerror = 'Ви були успішно зарєестровані!';
                                                    
                                            }
                                                  
                                    }
                                
                            }
                            
            }       
            
	}
	
	//
	// Виртуальный генератор HTML.
	//	
	protected function OnOutput()
	{
	   $this-> vars_right_bar = array('title'=>$this->title_rb,'pattern'=>$this->pattern, 'video'=>$this->video,'list_interesting_art'=>$this->interesting_articles,'title_int_articles'=>$this->title_int_art,'title_interv'=>$this->title_interv,'interv_list' =>$this->list_interview);
	   $vars = array('time' =>$this->main_time, 'title' =>$this->title,'content' => $this->news,'pattern'=>$this->pattern,'error_msg'=>$this->messageerror);	 
	   $this->content = $this->Template('v/v_user_registration.php', $vars);
        
		parent::OnOutput();
	}	
}
