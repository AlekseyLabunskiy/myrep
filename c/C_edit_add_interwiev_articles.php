<?php
//
// Контроллер страницы редактирования.
//
class C_edit_add_interwiev_articles extends C_Base_Editor
{
	protected $list_article_content;
	protected $article_content;	
	private $title_art;
	private $content_art;
	private $error_mesage;
	private $error;

	//
	// Конструктор.
	//
	function __construct()
	{	
		$this->article_content = array();	
	}
	
	//
	// Виртуальный обработчик запроса.
	//
	protected function OnInput()
	{
		parent::OnInput();
		
		$connect = M_connectDb::connectDb();
        $this->conn = $connect->connectmyDb();

	 	$mUsers = M_Users::Instance();
        $mUsers->ClearSessions();
        $user = $mUsers->Get();
        if (!$mUsers->Can('VIEW_EDIT'))
		{
			header('location:/index.php');
		}

		$this->title = $this->title . ' :: Редагування';
		
		if (isset($_GET['c']) && $_GET['c'] == 'edit_add_interwiev_articles') {
			if (isset($_POST['add_interv_article'])) {
				$this->error = '';
					$this->error_mesage = '';

					if ($_POST['add_title_interv_art'] == '') {
						$this->error = true;
						$this->error_mesage = 'Необхідно ввести заголовок новини';
					}
					if ($_POST['add_content_interv_article'] == '') {
						$this->error = true;
						$this->error_mesage = 'Необхідно ввести вміст новини';
					}
					if ($_POST['add_author_interv_art'] == '') {
						$this->error = true;
						$this->error_mesage = 'Необхідно ввести автора новини';
					}
						if (!$this->error) {
							$title = $_POST['add_title_interv_art'];
							$content = $_POST['add_content_interv_article'];
							$author = $_POST['add_author_interv_art'];
							$image = $_POST['add_image_interv_art'];
							$mArticles = M_Articles::Instance();
							if ($mArticles -> Add_interv($title, $content,$author,$image)) {
								$this->error_mesage = 'Новина успішно добавлена!';
									header('/index.php?c=edit_add_top_articles');								}
								
							}
							
						}
			}

		}
			
	protected function OnOutput()
	{
		$this->vars_content = array('error_mesage' => $this->error_mesage);	
		$this->content = $this->Template('v/v_edit_add_interv.php', $this->vars_content);
		parent::OnOutput();
	}	
}
