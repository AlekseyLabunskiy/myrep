<?php
class C_View_list_int_art extends C_Base
{
    function __construct()
	{	
	}
    
    protected function OnInput()
	{
	   parent::OnInput();
       $mVideo = M_Video::Instance();
       $this->video = $mVideo->All_video_limit();
       $mArticles = M_Articles::Instance();
       $this->interesting_articles = $mArticles->All_interesting_articles();
       
    }
	protected function OnOutput()
	{
		$vars = array('all_list_interesting_art'=>$this->interesting_articles,'pattern'=>$this->pattern);	 
		$this->content = $this->Template('v/v_list_int_art.php', $vars);
        $this-> vars_right_bar = array('title'=>$this->title_rb,'pattern'=>$this->pattern, 'video'=>$this->video);
        parent::OnOutput();
       
	}
}

?>