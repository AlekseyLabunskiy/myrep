<?php
//
// Контроллер страницы редактирования.
//
class C_Edit_top_articles extends C_Base_Editor
{
	protected $list_article_content;
	protected $article_content;	
	private $title_art;
	private $content_art;
    private $load_files;

	//
	// Конструктор.
	//
	function __construct()
	{	
		$this->article_content = array();	
	}
	
	//
	// Виртуальный обработчик запроса.
	//
	protected function OnInput()
	{
		parent::OnInput();
		
		$connect = M_connectDb::connectDb();
        $this->conn = $connect->connectmyDb();

	 	$mUsers = M_Users::Instance();
        $mUsers->ClearSessions();
        $user = $mUsers->Get();
        if (!$mUsers->Can('VIEW_EDIT'))
		{
			header('location:/index.php');
		}

		$this->title = $this->title . ' :: Редагування';
		
		if (isset($_GET['c']) && $_GET['c'] == 'edit_top_articles') {
			$mArticles = M_Articles::Instance();
			 $this ->list_article_content = $mArticles-> All_top_articles_no_limit();
		
			if (isset($_GET['id_top_art'])) {
				$mArticles = M_Articles::Instance();
			 	$id_article = trim(htmlspecialchars(stripcslashes($_GET['id_top_art'])));
					$this->cont = $mArticles -> Get_one_top_news($id_article);
						foreach ($this->cont as $cont) {
							$this->article_content['name'] = $cont['title_top_news'];
							$this->article_content['content'] = $cont['content_top_news'];
							$this->article_content['image'] = $cont['image_top_news'];
						}
			}
		}
			if (isset($_POST['edit_article'])) {
				$mArticles = M_Articles::Instance();
					$id_article = trim(htmlspecialchars(stripcslashes($_GET['id_top_art'])));
					$title = trim(htmlspecialchars(stripcslashes($_POST['title_art'])));
					$content = trim(htmlspecialchars(stripcslashes($_POST['content_article'])));
                    $image = '';
					if($mArticles-> Edit_top_artiles($id_article, $title, $content,$image)){
						header('location:/index.php?c=edit_top_articles');
						die();
					}

			}
    		if (isset($_GET['delete_top_article'])){
		        $id_article = trim(htmlspecialchars(stripcslashes($_GET['id_top_art'])));
                    $mArticles = M_Articles::Instance();
		                if ($mArticles->Delete_top_articles($id_article)){
			               header('location:/index.php?c=edit_top_articles');
			                   die();
		                }
            }
            if(isset($_POST['load_file'])){
                if($_FILES["filename"]["size"] = 0){
                    echo"загрузіть файл!";
                }
                
                if($_FILES["filename"]["size"] > 1024*3*1024){
                    echo ("Размер файла превышает три мегабайта");
                        exit;
                }
              if($mArticles->addImagesTopArticles()){
                 header('location:/index.php?c=edit_top_articles');
                    die();
              }
            }
              
            
}
	
	//
	// Виртуальный генератор HTML.
	//	
	protected function OnOutput()
	{
   	    $this->vars_content = array('content'=>$this ->list_article_content,'news_art'=>$this->article_content['name'],'content_art'=>$this->article_content['content'],'image_art'=>$this->article_content['image']);	
            $this->content = $this->Template('v/v_edit_top_articles.php', $this->vars_content );
		      parent::OnOutput();
	}	
}
?>