<?php
//
// Базовый контроллер сайта.
//
class C_View_interv extends C_Base
{
	protected $one_interv;		// содержание страницы
    protected $connect;
    protected $time;
    protected $title_interv;
    protected $list_interv;
    protected $title_int_articles;
    protected $list_int_art;
    protected $pattern;
	//
	// Конструктор.
	//
	function __construct()
	{
	}
	
	//
	// Виртуальный обработчик запроса.
	//
	protected function OnInput()
	{   
	   parent:: OnInput();
        $connect = M_connectDb::connectDb();
        $this->conn = $connect->connectmyDb();

        $mUsers = M_Users::Instance();
        $mUsers->ClearSessions();
        $user = $mUsers->Get();
        $this->pattern = '/([A-ZА-Я]+.+)[.!?]+[\s]+/sU';
       // дата
       $time = new main_Time;
            $this->main_time = $time-> mainTime();
       //переменные новостей         
	   $mArticles = M_Articles::Instance();
       
            if(isset($_GET['id_interv'])){
               $id_article =(int)$_GET['id_interv'];     
		          $this->one_interv = $mArticles->Get_one_interv_article($id_article);
            }
             $mComents = M_User_coments ::Instance();
            $this->list_coments = $mComents -> All_coments($id_news);
           
            if(isset($_POST['coments'])){
               $content = trim(htmlspecialchars(stripcslashes($_POST['add_coment'])));
               $mUsers = M_Users::Instance();
               $this->user = $mUsers->getUser();
               $name_user = $this->user['login'];
               $id_user = $this->user['id_user'];
            
            if($name_user == ''){$this->error_coment ='що б залишати коментарі треба авторизуватися';}
            
               $id_news = trim(htmlspecialchars(stripcslashes($_GET['id_interv'])));  
               $description = 'ONE_INTERVIEW';
               $mComents = M_User_coments ::Instance(); 
               if (!empty($_POST['add_coment'])) {
               $this->add_coment = $mComents -> Add_coment($content, $name_user, $id_news,$id_user,$description);
              }
           }
        $this->title_interv = 'ІНШІ ІНТЕРВ\'Ю';
        $this->list_interv = $mArticles -> All_interv_articles();
        
        $this->title_int_articles = 'КОРИСНО ЗНАТИ';
        $this ->list_int_art = $mArticles -> All_interesting_articles();
        
        
	}
	
	//
	// Виртуальный генератор HTML.
	//	
	protected function OnOutput()
	{
		$vars = array('time'=>$this->main_time, 'one_interv' => $this->one_interv,'right_bar' =>$this->right_bar,'list_coment'=> $this->list_coments,'error_coment'=>$this->error_coment);	
        $this->content = $this->Template('v/v_one_interv.php', $vars);
        $this-> vars_right_bar  = array('title_interv'=>$this->title_interv,'pattern'=>$this->pattern,'interv_list' =>$this->list_interv,'title_int_articles' => $this->title_int_articles,'list_interesting_art'=>$this ->list_int_art);
        
		parent::OnOutput();
	}	
}
?>