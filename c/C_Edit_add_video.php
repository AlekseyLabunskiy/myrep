<?php
//
// Контроллер страницы редактирования.
//
class C_Edit_add_video extends C_Base_Editor
{
	protected $list_article_content;
	protected $article_content;	
	private $title_art;
	private $content_art;
	private $error_mesage;
	private $error;

	//
	// Конструктор.
	//
	function __construct()
	{	
		$this->article_content = array();	
	}
	
	//
	// Виртуальный обработчик запроса.
	//
	protected function OnInput()
	{
		parent::OnInput();
		
		$connect = M_connectDb::connectDb();
        $this->conn = $connect->connectmyDb();

	 	$mUsers = M_Users::Instance();
        $mUsers->ClearSessions();
        $user = $mUsers->Get();
       
        if (!$mUsers->Can('VIEW_EDIT'))
		{
			header('location:/index.php');
		}

		$this->title = $this->title . ' :: Редагування';
		
		if (isset($_GET['c']) && $_GET['c'] == 'edit_add_video') {
			if (isset($_POST['add_video'])) {
				$this->error = '';
					$this->error_mesage = '';

					if ($_POST['add_title_video'] == '') {
						$this->error = true;
						$this->error_mesage = 'Необхідно ввести заголовок відео';
					}
					if ($_POST['add_link_video'] == '') {
						$this->error = true;
						$this->error_mesage = 'Необхідно ввести лінк відео';
					}
					if (!$this->error) {
						$title = $_POST['add_title_video'];
						$content = $_POST['add_link_video'];
						$mArticles = M_Articles::Instance();
						if ($mArticles -> Add_video($title, $content)) {
							$this->error_mesage = 'Вівдео успішно добавлено!';
								header('/index.php?c=edit_add_top_articles');								}
								
							}
							
						}
			}
		}
			
	protected function OnOutput()
	{
       	$this->vars_content = array('error_mesage' => $this->error_mesage);	
		$this->content = $this->Template('v/v_edit_add_video.php', $this->vars_content);

		parent::OnOutput();
	}	
}