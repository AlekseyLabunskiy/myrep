<?php

class C_User_inter extends C_Base
{
    private $news;
    private $main_time;
    private $video;
    private $interesting_articles;
    private $list_interview;

    
    //
    // Конструктор.
    //
    function __construct()
    {   
        $connect = M_connectDb::connectDb();
            $this->conn = $connect->connectmyDb();
    }
    
    //
    // Виртуальный обработчик запроса.
    //
    protected function OnInput()
    {
        parent::OnInput();
            $this->title = 'ВСІ НОВИНИ';
            $this->title_rb = 'ВІДЕО НОВИНИ';
            $this->title_int_art = 'КОРИСНО ЗНАТИ';
            $this->title_interv = 'ІНТЕРВ\'Ю';
              $this->pattern = '/([A-ZА-Я]+.+)[.!?]+[\s]+/sU';
            
            
            $mArticles = M_Articles::Instance();
            $this->news = $mArticles->All_articles();

            $this->interesting_articles = $mArticles->All_interesting_articles();
            $this->list_interview = $mArticles ->All_interv_articles();
           
            $time = new main_Time;
            $this->main_time = $time-> mainTime();
 
            $mVideo = M_Video::Instance();
            $this->video = $mVideo->All_video();
          
            $mUsers = M_Users::Instance();

            // Очистка старых сессий.
            $mUsers->ClearSessions();

            // Обработка отправки формы.
            if (!empty($_POST))
            {
                if ($mUsers->Login($_POST['inter_user_login'], 
                                   $_POST['inter_user_password'], 
                                   isset($_POST['remember'])))
                {
                    header('Location:/index.php');
                    die();
                }
            }    
                   
            
    }
    
    //
    // Виртуальный генератор HTML.
    //  
    protected function OnOutput()
    {
        $this-> vars_right_bar = array('title'=>$this->title_rb,'pattern'=>$this->pattern, 'video'=>$this->video,'list_interesting_art'=>$this->interesting_articles,'title_int_articles'=>$this->title_int_art,'title_interv'=>$this->title_interv,'interv_list' =>$this->list_interview);
        $vars = array('time' =>$this->main_time,'pattern'=>$this->pattern, 'title' =>$this->title,'content' => $this->news);  
        $this->content = $this->Template('v/v_user_inter.php', $vars);
        parent::OnOutput();
       
    }   
}
