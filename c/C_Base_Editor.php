<?php
//
// Контроллер страницы редактирования.
//
class C_Base_Editor extends C_Controller
{
	protected $list_article_content;
	protected $article_content;	
	private $title_art;
	private $content_art;
    private $title;
    private $user;
    protected $vars_content;
    protected $content;

	//
	// Конструктор.
	//
	function __construct()
	{	
		$this->article_content = array();	
	}
	
	//
	// Виртуальный обработчик запроса.
	//
	protected function OnInput()
	{
		parent::OnInput();
		$this->title = 'Консоль';
		$connect = M_connectDb::connectDb();
        $this->conn = $connect->connectmyDb();
        $this->vars_right_bar = '';
        $this->content = '';
	 	$mUsers = M_Users::Instance();
        $mUsers->ClearSessions();
        $this->user = $mUsers->Get();
        $this->online = $mUsers->IsOnline($id_user);
        if (!$mUsers->Can('VIEW_EDIT'))
		{
			header('location:/index.php');
		}

		$this->title = $this->title . ' :: Редагування';
		
	}
	
	//
	// Виртуальный генератор HTML.
	//	
	protected function OnOutput()
	{
        $this->edit_right_bar = $this ->Template('v/v_edit_right_bar.php');
            $this->vars = array('title' => $this->title, 'content' => $this->content,'right_bar' =>$this->edit_right_bar,'reg_user'=>$this->reg_user,'online' =>$this->online,'user'=>$this->user);	
                $page = $this->Template('v/v_main.php', $this->vars);	
        			
		echo $page;
	}	
}
